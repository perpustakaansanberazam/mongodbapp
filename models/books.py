from pymongo import MongoClient
from bson.objectid import ObjectId


class database:

    def __init__(self):
        try:
            self.nosql_db = MongoClient()
            self.db = self.nosql_db.perpustakaan
            self.mongo_col = self.db.books
            print("database connected")
        except Exception as e:
            print(e)

    def showBooks(self):
        try:
            result = self.mongo_col.find()
            return result
        except Exception as e:
            print(e)

    def showBookById(self, **params):
        try:
            bookId = params["id"]
            result = self.mongo_col.find_one({"_id": ObjectId(bookId)})
            return result
        except Exception as e:
            print(e)

    def searchBookByName(self, **params):
        try:
            key = params["name"]
            query = {"nama": {"$regex": key, "$options": "i"}}
            result = self.mongo_col.find(query)
            return result
        except Exception as e:
            print(e)

    def insertBook(self, document):
        try:
            self.mongo_col.insert_one(document)
            print("Data Added")
        except Exception as e:
            print(e)

    def updateBookById(self, bookId, newVal):
        try:
            getId = {"_id": ObjectId(bookId)}
            newValue = {"$set": {"genre": newVal}}
            self.mongo_col.update_one(getId, newValue)
            print("Data Updated")
        except Exception as e:
            print(e)

    def deleteBookById(self, bookId):
        try:
            getId = {"_id": ObjectId(bookId)}
            self.mongo_col.delete_one(getId)
            print("Data Deleted")
        except Exception as e:
            print(e)
