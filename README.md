# mongodbapp

Pada mongodbapp, terdapat modul API untuk mengakses data buku dengan endpoint sebagai berikut :


- router.get("/bookbyid)
dengan memberikan parameter "id" yang berisi bookid. Contoh routing :

http://127.0.0.1:8000/bookbyid

dengan parameter json :
{
    "id" : "606f04cfb65629dd95784782"
}

- router.get("/bookbyname")
dengan memberikan parameter "name" yang berisi nama buku. Contoh routing :

http://127.0.0.1:8000/bookbyname

dengan parameter json :
{
    "name" : "harry"
}

- router.get("/books")
tanpa perlu memberikan parameter. Contoh routing :
http://127.0.0.1:8000/bookbyname
