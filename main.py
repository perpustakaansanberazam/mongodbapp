from fastapi import FastAPI
from books_route import router as books_router

app = FastAPI()

app.include_router(books_router)


@app.get("/")
async def read_main():
    return {"message": "Hello Bigger Application"}


def add_book():
    data = open("bestsellers-with-categories.csv", encoding='utf-8')
    books = csv.reader(data, delimiter=',')
    next(books)

    for book in books:
        try:
            data = {
                "nama": book[0],
                "pengarang": book[1],
                "tahun terbit": book[5],
                "genre": book[6]
            }
            db.insertBook(data)
        except Exception as e:
            print("Kesalahan pada saat memasukkan data: {}".format(e))
            break


def search_books(params):
    for book in db.searchBookByName(params):
        pprint.pprint(book)


def get_books():
    for book in db.showBooks():
        pprint.pprint(book)


def get_booksId(params):
    pprint.pprint(db.showBookById(params))


def update_genre(params, newVal):
    db.updateBookById(params, newVal)
    get_booksId(params)


def delete_book(params):
    db.deleteBookById(params)
    get_books()
